function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1)); 
         # hidden_layer_size * (input_layer_size +1)

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));
         # num_labels * (hidden_layer_size +1)

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1)); # same size as Theta1
Theta2_grad = zeros(size(Theta2)); # same size as Theta2

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

# feed forward
X = [ones(m, 1) X]; # m * (input_layer_size+1)

z2 = X * Theta1'; # m * hidden_layer_size
a2 = sigmoid(z2); 

a2 = [ones(m, 1) a2]; # m * (hidden_layer_size + 1)

z3 = a2 * Theta2'; # m * num_labels
a3 = sigmoid(z3); # m * num_labels

# compute cost 
y_sparse = sparse (1:rows (y), y, 1); 
y_full = full(y_sparse);
J = sum(sum( -y_full.*log(a3) - (1-y_full).*log(1-a3) )) / m;

theta1Reg = Theta1(:, 2:end);
theta2Reg = Theta2(:, 2:end);

theta1Sum = sum(sum(theta1Reg.^2));
theta2Sum = sum(sum(theta2Reg.^2));

penal = lambda / (2*m) * (theta1Sum + theta2Sum);
J = J + penal;


# backpropagation
# ref: https://medium.com/secure-and-private-ai-math-blogging-competition/https-medium-com-fadymorris-understanding-vectorized-implementation-of-neural-networks-dae4115ca185
delta3 = a3 - y_full;  # m * num_labels

g_prime = sigmoidGradient([ones(m, 1) z2]); # m * (hidden_layer_size + 1)
delta2 = delta3*Theta2.*g_prime; # m * (hidden_layer_size + 1)

Delta2 = delta3'*a2; # num_labels * (hidden_layer_size+1)
Delta1 = delta2(:,2:end)'*X; # a1 = X; hidden_layer_size * (input_layer_size+1)

Theta2_without_bias = Theta2; 
Theta2_without_bias(:, 1) = 0;
Theta1_without_bias = Theta1;
Theta1_without_bias(:, 1) = 0;
Theta2_grad = Delta2/m + lambda /m * Theta2_without_bias;
Theta1_grad = Delta1/m + lambda /m * Theta1_without_bias;
## for loop solution
# delta2 = zeros(size(Theta2));
# for i = 1:m
  # delta2temp = delta3(i) * Theta2;
  # z2_exp = [1 z2(i)];
  # delta2i =  delta2temp .* sigmoidGradient(z2_exp);
  # delta2(i) = delta2i;
# end
# delta2 = delta_2(2:end);

# delta3 = sum(a3 - y_full);
# delta2 = (delta3 * Theta2) .* sigmoidGradient([ones(m, 1) z2]);
# delta2 = delta2(:, 2:end);

# Theta1_grad = delta2 * a1;
# Theta2_grad = delta3' * a2;

# delta = delta3 + sum(delta2));
# grad = delta / m;

% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
