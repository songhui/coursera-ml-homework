function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear 
%regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the 
%   cost of using theta as the parameter for linear regression to fit the 
%   data points in X and y. Returns the cost in J and the gradient in grad

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost and gradient of regularized linear 
%               regression for a particular choice of theta.
%
%               You should set J to the cost and grad to the gradient.
%


h = X * theta;
diff = h - y;
m2 = m*2;
J = diff' * diff / m2;

theta_without_bias = theta;
theta_without_bias(1,:) = 0;
panalty = theta_without_bias' * theta_without_bias * lambda / m2;
J = J + panalty;


grad = (X' * diff) / m + theta_without_bias*(lambda/m);


% =========================================================================

grad = grad(:);

end
